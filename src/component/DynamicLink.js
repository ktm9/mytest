import React from 'react'
import { Link } from 'react-router-dom'

 function DynamicLink() {
    const links=[
        {
          name:"Array Data",
          link:'/ArrayData'
        },
        {
          name:"Filter",
          link:'/filter'
        },
      
      ]
  return (
    <div>
      {
        links.map((item)=>(
            <div key={item.link}>
              <Link to ={`${item.link}`}>
              {item.name}
              </Link>
            </div>
    
          ))
      }
    </div>
  )
}

export default DynamicLink
