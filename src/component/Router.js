import { Route, createBrowserRouter, createRoutesFromElements } from "react-router-dom";
import ArrayData from "./ArrayData";
import DynamicLink from "./DynamicLink";
import { FilterComponent } from "./FilterComponent";

export const Routers=createBrowserRouter(
    createRoutesFromElements(
      

      <Route >
         <Route path="/" element={<DynamicLink/>}></Route>
         <Route path="/ArrayData" element={<ArrayData/>}></Route>
         <Route path="/filter" element={<FilterComponent/>}></Route>
        <Route />
        
      </Route>
    )
  );